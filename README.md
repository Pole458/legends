# Legends #

Hi! Welcome to **Legends**!

This is a **Work in Progress** game based on the lore of League of Legends.

You can find a gameplay video here:

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/op-Vvt1HF5c/0.jpg)](http://www.youtube.com/watch?v=op-Vvt1HF5c)

Download the [demo](https://bitbucket.org/Pole458/legends/downloads/Legends.zip).

## Story ##

This game tries to explain what happened before the Summoners War and how the League of Legends was born.
During the story, all the characters will develop into the Champions we all know.

During a period of peace between Demacia and Noxus, Katarina and Talon are sent on a secret mission in the Demacian territory.
Unfortunately, they will clash with Garen and his troops, making war break out again all over Runeterra.
To make matters worse, strange events are happening in the Shurima ruins, and someone is plotting to finally take revenge...

Will the Champions manage to restore the peace in Runeterra?

## Credits ##

Written, programmed and sprited by Pole.

Made with [DOTween](http://dotween.demigiant.com/index.php).