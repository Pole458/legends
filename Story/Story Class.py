

class Story(object):

    def __init__(self):
        self._events = []

    def addEvent(self, event):
        self._events.append(event)

    def getEvents(self):
        return self._events
    


class Event(object):

    def __init__(self, info, characters, prevEvents):
        self._info = info
        self._characters = characters
        self._prevEvents = prevEvents


    def getLevel(self):
        m = 0
        for e in self._prevEvents:
            m = max(e.getLevel(), m)
        return 1 + m

    def toString(self):
        return "_".join(self._info, self._characters, self._prevEvents)
