import os

def get_level(index):
    if index == 0:
       return 0
    else:
        return 1 + get_level( story[index][1] )

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def printStory():
    for i in range(len(story)):
        print( i, story[i][0])

def produceStory():
    #calculates every level and the maximum level
    global mx
    mx = 0
    for i in range(len(story)):
        story[i][2] = get_level( i )
        mx = max(mx, int(story[i][2]))

def readStory():
    
    with open("events.txt", "r") as f:
    
        s = f.readline()

        #charges all the events from the txt
        while(s != ""):
            s = s.split("_")
            story.append( [s[1], int(s[2]), 0] )
            s = f.readline()

    print ("All events charged!")
    

def writeStory():

    with open("story.txt", "w") as f:

        n = 0
        while( n <= mx ):
            f.write( str(n) )
            f.write("\n")
            print ( " -- ", n, " --")
            for i in range(len(story)):
                if story[i][2] == n:
                    f.write( "_".join( [str(i), story[i][0], str(story[i][1])] ) )
                    print( " - ".join( [str(i), story[i][0], str(story[i][1])] ) )
                    f.write("\n")
            n += 1

    print ("New story wrote!" )


def appendEvent():
    with open("events.txt","a") as f:
        f.write( "_".join( [str(len(story)-1), event, str(ref)] ) )
        f.write("\n")

def writeEvents():
    with open("events.txt","w") as f:
        for i in range(len(story)):
            f.write( "_".join( [ str(i), story[i][0], str(story[i][1]) ]))
            f.write("\n")

def writePreviousEvents():
    with open("prevEvents.txt","w") as f:
        for i in range(len(story)):
            f.write( "_".join( [ str(i), story[i][0], str(story[i][1]) ]))
            f.write("\n")

def getNewEvent():
    global event, ref
    event = input("New event: ")
    ref = int(input("This happens after: "))

def getInsertEvent():
    global event, ref
    event = input("Insrt event: ")
    ref = int(input("This happens before: "))

def newEvent():
    getNewEvent()

    story.append( [event,ref,0] )
    appendEvent()

    produceStory()
    
    cls()
    writeStory()


def changeRef(oldR, newR):
    for e in story:
        if e[1] == oldR:
            e[1] = newR

def insertEvent():
    getInsertEvent()

    r = story[ref][1]
    changeRef(story[ref][1], len(story))
    story.append( [event,r,0] )
    
    writeEvents()

    produceStory()

    cls()
    writeStory()

def deleteEvent():

    ref = int(input("Which event to remove? "))

    newR = story[ref][1]
    story.remove(story[ref])
    changeRef(ref, newR)
    writeEvents()

    produceStory()

    cls()
    writeStory()

def printMenu():

    global choice
    print ("---------")
    print ("0: add new event")
    print ("1: insert event")
    print ("2: delete event")
    print ("3: exit")

    choice = int(input(""))


#0: InsertIndex, 1: Event, 2: PreviousEventIndex\n -> .txt file
#0: Event ,1: PreviousEventIndex, 2: Level -> story list
story = []

event = "noone"
ref = 0
mx = 0

choice = -1

readStory()
writePreviousEvents()
produceStory()
writeStory()
printMenu()

while choice != 3:

    if choice == 0:
        newEvent()
    elif choice == 1:
        insertEvent()
    elif choice == 2:
        deleteEvent()

    printMenu()

