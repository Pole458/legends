﻿using UnityEngine;

/// <summary>
/// Class containing all the information needed for the each champion.
/// </summary>
[CreateAssetMenu(fileName = "ChampionData", menuName = "Data/Champion")]
public class ChampionData : ScriptableObject {

    public GameObject prefab;

    public Sprite icon;

    public Sprite spellIcon;

    public Sprite ultiIcon;
}
