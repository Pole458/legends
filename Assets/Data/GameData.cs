﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Data/Game")]
public class GameData : ScriptableObject {
	
    /// <summary>
    /// All the informations needed for each champion.
    /// </summary>
    public ChampionData[] championData;

	/// <summary>
	/// The number of sagas implemented in the game.
	/// </summary>
	public string[] sagasNames;

}

/*[CreateAssetMenu(fileName = "SagaData", menuName = "Data/Saga")]
public class SagaData : ScriptableObject {

	/// <summary>
	/// The name of this saga.
	/// </summary>
	public string sagaName;

	/// <summary>
	/// The chapters of this saga.
	/// </summary>
	public LevelData[] chapters;

}*/


