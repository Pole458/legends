﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SaveData {

	/// <summary>
	/// The gold owned by the player
	/// </summary>
	public int gold;

	/// <summary>
	/// The levels unlocked for each saga.
	/// </summary>
	public short[] levelsUnlocked;

	/// <summary>
	/// A boolean indicating if that champion is unlocked.
	/// </summary>
	public bool[] championsUnlocked;

	/// <summary>
	/// Creates a new Save file.
	/// </summary>
	public SaveData() {

		if (GameManager.instance != null) {

			gold = 0;

			levelsUnlocked = new short[GameManager.instance.gameData.sagasNames.Length];

			//Sets the first level of the first saga as unlocked
			levelsUnlocked [0] = 0;
			//Set all the others sagas as locked
			for (short i = 1; i < levelsUnlocked.Length; i++) {
				levelsUnlocked[i] = -1;
			}

			championsUnlocked = new bool[GameManager.instance.gameData.championData.Length];

			for (short i = 0; i < championsUnlocked.Length; i++) {
				championsUnlocked [i] = false;
			}
		}
	}
}
