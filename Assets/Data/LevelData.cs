﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "Data/Level")]
public class LevelData : ScriptableObject
{

    /// <summary>
    /// The levels needed to unlock this level;
    /// </summary>
    public LevelData[] levelsNeeded;

    /// <summary>
    /// The name of the champion which icon is used to display this level on the map in the Story Menu.
    /// </summary>
    public Sprite mapIcon;

    /// <summary>
    /// The icon position on the map in the Story Menu.
    /// </summary>
    public Vector2 iconPosition;

    /// <summary>
    /// The data of the champion used in this level.
    /// </summary>
    public ChampionData[] championData;

}