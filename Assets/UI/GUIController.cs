﻿using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class GUIController {

    public GameObject gameObject;

    /// The champion Icon for the current champion controlled by the player
    public Image mainChampionIcon;

    /// <summary>
    /// The main champion HP bar.
    /// </summary>
    public RectTransform mainChampionBar;

    /// The championHUDs for the other champions in the pool
    public Image[] championHUDs;

    /// <summary>
    /// The champions HP bars.
    /// </summary>
    public RectTransform[] championsBars;

    /// <summary>
    /// The icon for the spell CD.
    /// </summary>
    public Image spellIcon;

    public GUIController() {

        // Grabs the "GUI" gameobject.
        gameObject = LevelManager.instance.transform.FindChild("GUI").gameObject;

        // Grabs the MainChampion icon and HP bar.
        mainChampionIcon = gameObject.transform.FindChild("MainChampionHUD").GetComponent<Image>();
        mainChampionBar = gameObject.transform.FindChild("MainChampionHUD").GetChild(1).GetComponent<RectTransform>();

        // Grabs the others champion icons and HP bars.
        championHUDs = new Image[gameObject.transform.childCount - 2];
        championsBars = new RectTransform[gameObject.transform.childCount - 2];
        for (int i = 0; i < championHUDs.Length; i++) {
            championHUDs[i] = gameObject.transform.GetChild(i + 1).GetComponent<Image>();
            championsBars[i] = gameObject.transform.GetChild(i + 1).GetChild(1).GetComponent<RectTransform>();
        }

        // Grabs the Spell icon.
        spellIcon = gameObject.transform.GetChild(gameObject.transform.childCount - 1).GetComponent<Image>();

        // ---

        // Assingn the right sprites.
        mainChampionIcon.sprite = LevelManager.instance.levelData.championData[LevelManager.instance.currentChampion].icon;

        for (int i = 0; i < LevelManager.instance.championPool.Length - 1; i++) {
            // Disable the champions not used.
            LevelManager.instance.championPool[i + 1].gameObject.SetActive(false);

            championHUDs[i].gameObject.SetActive(true);
            championHUDs[i].sprite = LevelManager.instance.levelData.championData[i + 1].icon;
        }

        // Disables the HUDs not used.
        for (int i = LevelManager.instance.levelData.championData.Length; i < gameObject.transform.childCount - 1; i++)
            championHUDs[i - 1].gameObject.SetActive(false);

        // Sets the spell Icon.
        spellIcon.sprite = LevelManager.instance.levelData.championData[LevelManager.instance.currentChampion].spellIcon;

    }

    public void Update() {

        // Grabs ChangeChampion input.
        if (Input.GetButtonDown("ChangeChampion") && LevelManager.instance.CanChangeChampion() && Player.Champion().lastState == "Idle") {
            ChangeChampion();
        }

        // Update MainChampion HP bar.
        mainChampionBar.localScale = new Vector3(Player.Champion().HPPerc(), 1, 1);

        // Updates SpellIcon.
        spellIcon.fillAmount = Player.Champion().GetSpellCD();
    }

    private void ChangeChampion() {

        // Gets the information about the current champion.
        Vector2 pos = Player.Champion().transform.position;
        bool faceRight = Player.Champion().IsFacingRight();
        Transform parent = Player.GameObject().transform.parent;

        // Disables the current champion.
        Player.GameObject().SetActive(false);
        // Increments "currentChampion" value.
        LevelManager.instance.currentChampion = (short)((LevelManager.instance.currentChampion + 1) % LevelManager.instance.championPool.Length);

        // Enables the new current champion.
        Player.GameObject().SetActive(true);

        // Move the new currentChampion in the right position.
        Player.Champion().transform.position = pos;

        // Flip the champion if needed.
        if (Player.Champion().IsFacingRight() != faceRight)
            Player.Champion().Flip();

        // Sets the right parent.
        Player.GameObject().transform.parent = parent;

        // Change the HUDs.
        mainChampionIcon.sprite = LevelManager.instance.levelData.championData[LevelManager.instance.currentChampion].icon;
        spellIcon.sprite = LevelManager.instance.levelData.championData[LevelManager.instance.currentChampion].spellIcon;

        for (short i = 1; i < LevelManager.instance.championPool.Length; i++) {
            // Icon.
            championHUDs[i - 1].sprite = LevelManager.instance.levelData.championData[(i + LevelManager.instance.currentChampion) % LevelManager.instance.levelData.championData.Length].icon;
            // HP bar.
            championsBars[(i - 1) * 2].localScale = new Vector3(LevelManager.instance.championPool[(i + LevelManager.instance.currentChampion) % LevelManager.instance.championPool.Length].HPPerc(), 1, 1);
        }

        // Tells the camera controller to follow the new current main champion.
        LevelManager.instance.cameraController.SetTargetTransform(Player.GameObject());

    }
}
