﻿using UnityEngine;

public class ButtonListener : MonoBehaviour {

	/// <summary>
	/// Tells the GameManager which button was pressed.
	/// </summary>
	/// <param name="buttonName">Button name.</param>
	public void OnButtonPress(string buttonName) {
		GameManager.instance.OnButtonPress (buttonName);
	}
}
