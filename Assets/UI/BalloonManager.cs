﻿using System;
using UnityEngine;
using DG.Tweening;

public class BalloonManager : MonoBehaviour {

    public float letterPause = 0.005f;

    public Transform balloonSquare;

    public Transform balloonTriangle;

    public TextMesh textMesh;

    private void Awake() {
        // Disables the balloon when starting the level (in case it was still enabled for testing).
        gameObject.SetActive(false);
    }

    public void WriteMessage(Sequence sequence, Vector2 pos, string message) {

        message = message.Replace("\\n", Environment.NewLine);

        //Enabled the balloon, sets the size and the position.
        sequence.AppendCallback(() => {
            gameObject.SetActive(true);
            SetBalloon(message);
            transform.position = pos;
        });

        // Tweeen the text.
        sequence.Join(DOTween.To(() => "", x => textMesh.text = x, message, message.Length * letterPause).SetOptions(true).SetEase(Ease.Linear));

        sequence.AppendCallback(() => sequence.Pause()).AppendInterval(2f / Application.targetFrameRate);

    }

    public void WriteMessage(Sequence sequence, SpriteRenderer sprite, string message) {

        message = message.Replace("\\n", Environment.NewLine);

        //Enabled the balloon, sets the size and the position.
        sequence.AppendCallback(() => {
            gameObject.SetActive(true);
            SetBalloon(message);
            transform.position = sprite.bounds.center + new Vector3(0, 0.25f + sprite.bounds.extents.y + textMesh.GetComponent<Renderer>().bounds.extents.y);
        });

        // Tweeen the text.
        sequence.Join(DOTween.To(() => "", x => textMesh.text = x, message, message.Length * letterPause).SetOptions(true).SetEase(Ease.Linear));

        sequence.AppendCallback(() => sequence.Pause()).AppendInterval(2f / Application.targetFrameRate);

    }

    public void SetBalloon(string s) {

        textMesh.text = s;
        balloonSquare.localScale = textMesh.GetComponent<Renderer>().bounds.size + new Vector3(0.5f, 0);
        textMesh.transform.position = new Vector2(transform.position.x, transform.position.y + textMesh.GetComponent<Renderer>().bounds.extents.y);
        textMesh.text = "";

    }

    /*private void SetTriangleRotation(GameObject speaker) {

        if(speaker != null) {
            Debug.Log(speaker.name + " position is: " + speaker.transform.position);
            Quaternion newRotation = Quaternion.LookRotation(speaker.transform.position - balloonTriangle.position, Vector3.forward);
            newRotation.x = 0;
            newRotation.y = 0;
            balloonTriangle.rotation = newRotation;

            balloonTriangle.localScale = new Vector3(balloonTriangle.localScale.x, -Vector2.Distance(balloonTriangle.position, speaker.transform.position) * 0.8f, balloonTriangle.localScale.z);

            balloonTriangle.position = new Vector3(balloonTriangle.localScale.x, -Vector2.Distance(balloonTriangle.position, speaker.transform.position) * 0.8f, balloonTriangle.localScale.z);

        } else {
            Debug.Log("No speaker found");
            balloonTriangle.localScale = new Vector3(balloonTriangle.localScale.x, 0, balloonTriangle.localScale.z);
        }
    }*/

}
