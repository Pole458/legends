﻿using UnityEngine;
using Utilities;
using DG.Tweening;


public class DemaciaVsNoxusAnim : AnimationData {

    public CameraController cameraController;
    public BalloonManager balloon;
    public Champion katarina;
    public Champion talon;

    public Minion minionCaster;

    public TutorialSpell spellTutorial;

    public override void Play(int i) {

        Sequence s = DOTween.Sequence();

        s.OnStart(LevelManager.instance.OnBeginAnimation);
        s.OnComplete(LevelManager.instance.OnEndAnimation);

        switch (i) {

            case 0:

                s.AppendCallback(() => {
                    LevelManager.instance.SetChangeChampion(false);
                    LevelManager.instance.SetSpellUse(false);
                    // Disables the mainChampionUI.
                    LevelManager.instance.guiController.mainChampionIcon.gameObject.SetActive(false);
                });

                break;
            case 1:

                // Katarina stops
                s.AppendCallback(() => katarina.GetRigidBody().velocity = Vector2.zero).AppendInterval(1);

                // Katarina looks the other way
                s.AppendCallback(() => katarina.Flip()).AppendInterval(0.75f);

                // Katarina looks the other way again
                s.AppendCallback(() => katarina.Flip()).AppendInterval(0.75f);

                // Katarina says:
                balloon.WriteMessage(s, new Vector2(3.5f, 3.7f), "Where is Talon?\nHe must be already gone!");
                balloon.WriteMessage(s, new Vector2(-3, 3f), "I have to hurry up!");
                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                break;
            case 2:

                // Katarina stops.
                s.AppendCallback(() => katarina.GetRigidBody().velocity = Vector2.zero).AppendInterval(0.25f);

                // Camera moves to minion.
                s.Append(cameraController.transform.DOMove(new Vector2(0.5f, 10), 1.25f));

                // Katarina says:
                balloon.WriteMessage(s, new Vector2(0.5f, 12), "A Minion!");
                balloon.WriteMessage(s, new Vector2(2, 10), "Luckily it can't see me\nbehind this trunk");
                balloon.WriteMessage(s, new Vector2(-2, 9.5f), "I should eliminate it before\nit will reveal my presence\nto the Demacian army");
                balloon.WriteMessage(s, new Vector2(-2, 9.5f), "I need to get\na little bit\ncloser...");

                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                // Camera goes back to Katarina.
                s.Append(cameraController.transform.DOMove(Player.Position(), 0.75f));
                break;
            case 3:
                // Katarina stops.
                s.AppendCallback(() => katarina.GetRigidBody().velocity = Vector2.zero);

                // Katarina says:
                balloon.WriteMessage(s, new Vector2(3.5f, 9), "Now I'm close enough!\nLet's strike!");
                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                s.AppendCallback(() => LevelManager.instance.SetSpellUse(true));
                s.AppendCallback(() => LevelManager.instance.setMove(false));
                s.AppendCallback(() => spellTutorial.TurnOn());

                break;
            case 4:

                s.AppendCallback(() => LevelManager.instance.setMove(true));
                s.AppendCallback(() => spellTutorial.TurnOff());
                s.AppendInterval(0.75f);

                // Katarina says:
                balloon.WriteMessage(s, new Vector2(3.5f, 13), "Great!");
                balloon.WriteMessage(s, new Vector2(3.5f, 13), "Let's reach Talon!");
                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                break;
            case 5:

                // Disable and stop the minion
                s.AppendCallback(() => {
                    minionCaster.enabled = false;
                    minionCaster.GetRigidBody().velocity = Vector2.zero;
                    if (!minionCaster.IsFacingRight()) minionCaster.Flip();
                });

                // Katarinas walks west.
                s.Append(MoveUnit(katarina, new Vector2(-9.5f, 12)));

                // Camera shows minions on the left.
                s.Join(cameraController.transform.DOMove(new Vector2(-13, 13), 1));

                balloon.WriteMessage(s, new Vector2(-12, 11.5f), "LOOK OUT!");
                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                s.AppendCallback(() => minionCaster.GetAnimator().SetTrigger("Attack"));

                s.AppendInterval(0.5f);

                // Set up Talon.
                s.AppendCallback(() => talon.enabled = false);
                s.AppendCallback(() => talon.GetCollider2D().enabled = false);
                s.AppendCallback(() => talon.gameObject.SetActive(true));
                s.AppendCallback(() => talon.transform.position = new Vector2(-9.5f, 10));
                s.AppendCallback(() => {
                    if (talon.IsFacingRight()) talon.Flip();
                });

                // Talon dashes to save Katarina.
                s.Append(talon.GetRigidBody().DOMove(new Vector2(-9.5f, 13), 0.75f)).SetEase(Ease.Linear);

                // Katarina gets pushed in the bush.
                s.Insert(s.Duration() - 0.5f, katarina.GetRigidBody().DOMove(new Vector2(-9.5f, 13), 0.25f)).SetEase(Ease.Linear);

                s.AppendCallback(() => talon.GetCollider2D().enabled = true);

                s.AppendCallback(() => minionCaster.enabled = true);

                s.Append(MoveUnit(katarina, new Vector2(-10.5f, 13)));

                s.AppendCallback(() => katarina.Flip());

                balloon.WriteMessage(s, new Vector2(-11, 15), "Talon!");
                balloon.WriteMessage(s, new Vector2(-10, 15), "Katarina, what are you doing?");
                balloon.WriteMessage(s, new Vector2(-12, 15), "I'm sorry master...\nI was trying to reach you");
                balloon.WriteMessage(s, new Vector2(-10.5f, 15.5f), "You won't became part of the\nCrimson Elite\nif you keep on\nforgetting the most important thing!");
                balloon.WriteMessage(s, new Vector2(-9.75f, 15), "Never lower the guard!");
                balloon.WriteMessage(s, new Vector2(-12.5f, 15), "I know... I'm sorry,\nit won't happen anymore");
                balloon.WriteMessage(s, new Vector2(-10, 15.5f), "Now let's eliminate\nthis minion.\nIt can't see us\nwhile we are in the bush");
                s.AppendCallback(() => katarina.Flip());
                balloon.WriteMessage(s, new Vector2(-10, 15), "Do NOT get hit by its bullet, ok?");
                balloon.WriteMessage(s, new Vector2(-10, 15), "I don't want to\nhear you\nwhining about the pain");
                balloon.WriteMessage(s, new Vector2(-12.5f, 15.5f), "Don't worry Talon,\nI will dodge every bullet!");
                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                s.AppendCallback(() => LevelManager.instance.HPThreshold = 50);

                break;

            case 6:

                s.AppendCallback(() => LevelManager.instance.HPThreshold = 0);

                s.AppendInterval(1);

                // Flip Katarina in Talon's direction.
                s.AppendCallback(() => {
                    if ((katarina.transform.position.x > talon.transform.position.x && katarina.IsFacingRight()) || (katarina.transform.position.x < talon.transform.position.x && !katarina.IsFacingRight()))
                        katarina.Flip();
                });

                balloon.WriteMessage(s, katarina.GetSpriteRenderer(), "Did you see? It was easy!");
                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                float t = s.Duration();

                s.Append(MoveUnit(talon, new Vector3(-13.5f, 13)));
                s.Join(MoveUnit(katarina, new Vector2(-15, 13)).OnComplete(() => {
                    if (!katarina.IsFacingRight())
                        katarina.Flip();
                }));

                s.Join(cameraController.transform.DOMove(new Vector3(-15, 13) + cameraController.offset, s.Duration() - t));

                balloon.WriteMessage(s, talon.GetSpriteRenderer(), "Mhh, let me check");

                // Shows the HP bar
                s.AppendCallback(() => {
                    LevelManager.instance.guiController.mainChampionIcon.gameObject.SetActive(true);
                    LevelManager.instance.guiController.spellIcon.gameObject.SetActive(false);
                    LevelManager.instance.guiController.gameObject.SetActive(true);
                });

                if (katarina.HP > 90)
                    balloon.WriteMessage(s, talon.GetSpriteRenderer(), "Yeah, that wasn't so bad");
                else
                    balloon.WriteMessage(s, talon.GetSpriteRenderer(), "You should pay more attention");

                balloon.WriteMessage(s, talon.GetSpriteRenderer(), "You can't get hit too many times.\nSo, try to dodge as much as you can");
                balloon.WriteMessage(s, talon.GetSpriteRenderer(), "Let's move on, we must continue your training");
                balloon.WriteMessage(s, talon.GetSpriteRenderer(), "Remember, if you need my help, just press 2");
                s.AppendCallback(() => balloon.gameObject.SetActive(false));

                s.Append(MoveUnit(talon, new Vector2(-15, 13.1f)));

                s.AppendCallback(() => {
                    talon.enabled = true;
                    talon.gameObject.SetActive(false);
                    // Enables the spellIcon.
                    LevelManager.instance.guiController.spellIcon.gameObject.SetActive(true);
                    // Now the player can switch champion.
                    LevelManager.instance.SetChangeChampion(true);
                });

                break;
            case 7:

                // Stop the player.
                s.AppendCallback(() => Player.Champion().GetRigidBody().velocity = Vector2.zero).AppendInterval(1);

                // Write final message.
                balloon.WriteMessage(s, Camera.main.transform.position, "GG");
                balloon.textMesh.alignment = TextAlignment.Left;
                balloon.WriteMessage(s, Camera.main.transform.position, "Thanks for playing this demo!\n\n-Pole");

                // Close the game.
                s.AppendCallback(() => {
                    balloon.gameObject.SetActive(false);
                    Application.Quit();
                });

                break;
        }

        LevelManager.instance.mainSequence = s;
    }

}
