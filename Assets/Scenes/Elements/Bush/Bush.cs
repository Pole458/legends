﻿using UnityEngine;

public class Bush : MonoBehaviour {

    private SpriteRenderer spriteRenderer;

    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void SetVisible(bool b) {

        Color c = spriteRenderer.color;
        c.a = 1;

        if (b)
            c.a = 1;
        else
            c.a = 0.5f;

        spriteRenderer.color = c;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<Champion>().SetVisible(false);
            SetVisible(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<Champion>().SetVisible(true);
            SetVisible(true);
        }
    }

}
