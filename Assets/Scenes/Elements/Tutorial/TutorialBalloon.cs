﻿using UnityEngine;
using DG.Tweening;

public class TutorialBalloon : MonoBehaviour {

    public SpriteRenderer mouse;

    private Tween fadingTween;

    public float timer;

    private float nextTime;

    private void Awake() {

       fadingTween = mouse.DOFade(0.75f, 1).SetLoops(-1, LoopType.Yoyo).Pause();

    }

    private void Update() {

        if (LevelManager.instance.balloon.gameObject.activeSelf) {

            if (LevelManager.instance.mainSequence.IsPlaying()) {
                nextTime = Time.time + timer;
            }

            if (Time.time > nextTime) {
                TurnOn();
            } else TurnOff();

        }
    }

    public void TurnOn() {

        // Reposition itself in the bottom-right of the balloon.
        transform.position = new Vector2(LevelManager.instance.balloon.textMesh.GetComponent<Renderer>().bounds.max.x, LevelManager.instance.balloon.textMesh.GetComponent<Renderer>().bounds.min.y);
        
        fadingTween.Play();
    }

    public void TurnOff() {
        fadingTween.Rewind();
    }

}
