﻿using UnityEngine;
using DG.Tweening;

public class TutorialSpell : MonoBehaviour {

    public SpriteRenderer redCirle, mouse;

    private Tween[] fadingTween;

    private void Awake() {

        fadingTween = new Tween[2];

        fadingTween[0] = redCirle.DOFade(0.4f, 1).SetLoops(-1, LoopType.Yoyo).Pause();
        fadingTween[1] = mouse.DOFade(0.75f, 1).SetLoops(-1, LoopType.Yoyo).Pause();
    }

    public void TurnOn() {
        gameObject.SetActive(true);
        fadingTween[0].Play();
        fadingTween[1].Play();
    }

    public void TurnOff() {
        gameObject.SetActive(false);
        fadingTween[0].Kill();
        fadingTween[1].Kill();

    }

}
