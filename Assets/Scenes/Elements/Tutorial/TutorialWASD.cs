﻿using UnityEngine;
using Utilities;
using DG.Tweening;

public class TutorialWASD : MonoBehaviour {

    public SpriteRenderer W, A, S, D;

    public float firstTimer;

    public float pressedTimer;

    private float nextTimePressed;

    private Tween[] fadingTween;

    private void Awake() {

        nextTimePressed = Time.time + firstTimer;

        fadingTween = new Tween[4];

        fadingTween[0] = W.DOFade(0.5f, 1).SetLoops(-1, LoopType.Yoyo).Pause();
        fadingTween[1] = A.DOFade(0.5f, 1).SetLoops(-1, LoopType.Yoyo).Pause();
        fadingTween[2] = S.DOFade(0.5f, 1).SetLoops(-1, LoopType.Yoyo).Pause();
        fadingTween[3] = D.DOFade(0.5f, 1).SetLoops(-1, LoopType.Yoyo).Pause();
    }

    private void Update() {


        if (LevelManager.instance.CutScene() || !LevelManager.instance.CanMove() || Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0) {
            nextTimePressed = Time.time + pressedTimer;
            SetKeysActive(false);

        }

        if (Time.time > nextTimePressed) {

            SetKeysActive(true);
            transform.position = Player.Position();

        }
    }

    private void SetKeysActive(bool b) {

        W.gameObject.SetActive(b);
        A.gameObject.SetActive(b);
        S.gameObject.SetActive(b);
        D.gameObject.SetActive(b);

        if (b) {

            fadingTween[0].Play();
            fadingTween[1].Play();
            fadingTween[2].Play();
            fadingTween[3].Play();

        } else {

            fadingTween[0].Pause();
            fadingTween[1].Pause();
            fadingTween[2].Pause();
            fadingTween[3].Pause();

        }

    }


}
