﻿using UnityEngine;

public class MovingPlatform : MonoBehaviour {

	public Vector2[] path;

	private int targetPoint;

	public bool backAndForth;

	public float speed;

	private int direction;

    private Collider2D thisCollider;

    private void Awake() {
        targetPoint = 0;
        direction = 1;
        thisCollider = GetComponent<Collider2D>();
    }

    private void Update() {

        if (Vector2.Distance(transform.position, path[targetPoint]) < 0.1f) {
            targetPoint += direction;
            if (backAndForth) {
                if (targetPoint <= -1 && direction == -1) {
                    direction = 1;
                    targetPoint = path.Length - 1;
                } else if (targetPoint >= path.Length && direction == 1) {
                    direction = -1;
                    targetPoint = 0;
                }
            }
        }
    }

    private void FixedUpdate() {
        transform.position = Vector2.MoveTowards(transform.position, path[targetPoint], speed * Time.fixedDeltaTime);
    }

    public Bounds GetBounds() {
       
        float minX = float.PositiveInfinity;
        float maxX = float.NegativeInfinity;

        float minY = float.PositiveInfinity;
        float maxY = float.NegativeInfinity;

        for (int i=0; i<path.Length; i++) {
            minX = Mathf.Min(minX, path[i].x);
            maxX = Mathf.Max(maxX, path[i].x);

            minY = Mathf.Min(minY, path[i].y);
            maxY = Mathf.Max(maxY, path[i].y);
        }

        return new Bounds(new Vector2((maxX + minX) / 2, (maxY + minY) / 2), new Vector2((maxX - minX) / 2, (maxY - minY) / 2));
    }

    private void OnCollisionEnter2D(Collision2D collisionInfo) {

        //If the game object is a unit...
        if (collisionInfo.gameObject.GetComponent<Unit>() != null) {
            //If it is above us
            if (collisionInfo.collider.bounds.min.y <= thisCollider.bounds.max.y + 0.02f) {
                collisionInfo.transform.parent = transform;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collisionInfo) {
        // Set every child that leaves this platform as a sibling to prevent this moving them.
        if (collisionInfo.transform.parent == transform)
            collisionInfo.transform.parent = LevelManager.instance.transform;
    }

    public void OnDrawGizmosSelected() {
	
		// Display the path of this moving platform
		Gizmos.color = new Color(0, 1, 0);
		for(short i=0; i<path.Length-1; i++)
			Gizmos.DrawLine (path[i], path[i+1]);

		//If the path is a close line, connect the first and the last point;
		if(!backAndForth)
			Gizmos.DrawLine (path[0], path[path.Length-1]);

	}

    private void OnValidate() {
        // Automatically sets the position of this platform in the first point of the path.
        if(path.Length > 0)
            transform.position = path[0];

    }
}
