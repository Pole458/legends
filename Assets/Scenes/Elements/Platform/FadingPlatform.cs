﻿using UnityEngine;

public class FadingPlatform : MonoBehaviour {

	/// <summary>
	/// The 3 modes selectable for the fading platforms.
	/// </summary>
	public enum Mode {time, contact, random};

	/// <summary>
	/// The selected mode for this platform.
	/// </summary>
	public Mode mode;

	/// <summary>
	/// How much times passes before the platform fades [seconds].
	/// </summary>
	public float fadeTime;
	/// <summary>
	/// How much time does the platform blink [seconds].
	/// </summary>
	public float blinkTime;

	/// <summary>
	/// The random offset [seconds].
	/// </summary>
	public float randomOffset;

	private Animator thisAnimator;

    private void Awake() {
        thisAnimator = GetComponent<Animator>();

        if (mode == Mode.contact) {
            thisAnimator.SetFloat("FadeSpeed", 0);
        } else if (mode == Mode.random) {
            thisAnimator.SetFloat("FadeSpeed", 1 / (fadeTime + Random.Range(-randomOffset, randomOffset)));
        } else
            thisAnimator.SetFloat("FadeSpeed", 1 / fadeTime);

        thisAnimator.SetFloat("BlinkSpeed", 0.5f / blinkTime);
    }

    private void OnCollisionEnter2D() {

        // Starts fading when touched.
        if (mode == Mode.contact) {
            thisAnimator.SetTrigger("Fade");
        }
    }

    public void OnFadeExit() {
		if (mode == Mode.contact) {
            gameObject.SetActive(false);
            // Before disabling itself, removes all his children and sets them as child of LevelManager.
            foreach (Transform child in transform) {
                child.parent = LevelManager.instance.transform;
            }
        } else if (mode == Mode.random)
			thisAnimator.SetFloat ("FadeSpeed", 1 / (fadeTime + Random.Range (-randomOffset, randomOffset)));
	}

    private void OnValidate() {
		if (mode == Mode.contact) {
			fadeTime = -1;
			randomOffset = 0;
		} else if (mode == Mode.time) {
			randomOffset = 0;
		} else if (mode == Mode.random) {
			if (randomOffset < 0)
				randomOffset = 0;
		}
	}
}
