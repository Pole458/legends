﻿using UnityEngine;

public class Katarina : Champion {

    private Hitbox hitBox;

    public int spellDamage;
    public float spellKnockBack;
    public float spellRange;

    private Vector2 spellTargetPosition;

	public AudioSource eOnCast;
	public AudioSource rOnCast;
	public AudioSource rLoop;

    protected override void Awake() {

        base.Awake();

        hitBox = transform.GetChild(0).GetComponent<Hitbox>();
    }

    protected override void Update() {
      
        base.Update();

        // Stops the ulti loop sound once the animation is finished or interrupted.
            if (rLoop.isPlaying && !animator.GetCurrentAnimatorStateInfo(0).IsName("Ulti"))
            rLoop.Stop();

        if (!LevelManager.instance.CutScene()) {
            CheckStatus();
        } else
            PuppetBehaviour();

    }

    protected override void CheckStatus() {

		if( animator.GetCurrentAnimatorStateInfo(0).IsName("Spell") )
			DuringSpell();
		else if( animator.GetCurrentAnimatorStateInfo(0).IsName("Ulti") )
			DuringUlti();
		else
			base.CheckStatus();
	}

	protected override void DuringIdle() {

        if (SpellCondition()) { //Spell

            spellTargetPosition = GetSpellTargetPosition();

            if (IsSpellTargetLegit())
                animator.SetTrigger("Spell");

        } /*else if (Input.GetButtonDown("Ulti")) { //Ulti

            animator.SetTrigger("Ulti");

        }*/ else
            base.DuringIdle();
    }

    /// <summary>
    /// Checks if the player clicked on a forbid point (i.e. outside the map).
    /// </summary>
    /// <returns></returns>
    private bool IsSpellTargetLegit() {
        return LevelManager.instance.roomManager.GetCurrentRoom().transform.Find("Floor").GetComponent<Collider2D>().OverlapPoint(spellTargetPosition);
    }

    private Vector2 GetSpellTargetPosition() {

        Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // If the player clicked too far, set the target position as the max range.
        if (Vector2.Distance(target, rigidBody.position) > spellRange)
            target = (target - rigidBody.position).normalized * spellRange + rigidBody.position;
   
        return target;
    }

	private void SpellOnCast() {

        nextSpell = Time.time + spellCD;

        hitBox.damage = spellDamage;
        hitBox.knockback = spellKnockBack;

        rigidBody.velocity = Vector2.zero;

        // Flips Katarina after the Shunpo.
        if (spellTargetPosition.x - transform.position.x > 0 && IsFacingRight())
            Flip();
        else if (spellTargetPosition.x - transform.position.x < 0 && !IsFacingRight())
            Flip();

        // Blinks to target location.
        transform.position = spellTargetPosition;

		// Audio.
		eOnCast.Play();
	}

	private void DuringSpell() {

        lastState = "Spell";

       /* if ( Input.GetButtonDown("Ulti") ) {
			animator.SetTrigger("Ulti");
		} else {
			lastState = "Spell";
		}*/
	}

	private void UltiOnCast() {

		rigidBody.velocity = Vector2.zero;
		// Audio.
		rOnCast.Play();
		rLoop.Play();
	}

	private void DuringUlti() {

		if (lastState != "Ulti")
			UltiOnCast();

        DirectionInput();
        lastState = "Ulti";

    }
	
}
