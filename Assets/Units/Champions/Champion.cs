﻿using UnityEngine;

public class Champion : Unit {

    /// <summary>
    /// The CoolDown of the Spell of this champion.
    /// </summary>
    public float spellCD;
    protected float nextSpell;

    protected bool visible;

    protected override void Awake () {

        base.Awake();

        nextSpell = float.NegativeInfinity;

        visible = true;
    }

	protected override void Update () {

        base.Update();

        animator.SetBool("Moving", rigidBody.velocity != Vector2.zero);

    }

    protected override void PuppetBehaviour() {

        base.PuppetBehaviour();

    }

    protected virtual void CheckStatus() {

		if( animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") || animator.GetCurrentAnimatorStateInfo(0).IsName("Run") )
			DuringIdle();
	}

    protected void DirectionInput() {

        if (LevelManager.instance.CanMove())
            rigidBody.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized * speed;
        else
            rigidBody.velocity = Vector2.zero;
    }

    protected bool SpellCondition() {

        return LevelManager.instance.CanUseSpell() && Input.GetMouseButtonDown(0) && nextSpell <= Time.time;

    }

	protected virtual void DuringIdle() {

        DirectionInput();
        CheckDirection();

        lastState = "Idle";
    }

    public virtual float GetSpellCD() {
        return 1 - (nextSpell - Time.time) / spellCD;
    }

    public override void TakeDamage(int damage) {

        // If this champion is still in the "Hit" animation, he can't take damage.
        if (!animator.GetCurrentAnimatorStateInfo(1).IsName("Hit"))
            base.TakeDamage(damage);

        if (LevelManager.instance.HPThreshold > 0)
            HP = Mathf.Max(HP, LevelManager.instance.HPThreshold);

    }

    public bool IsVisible() {
        return visible;
    }

    public void SetVisible(bool b) {

        visible = b;

        Color c = spriteRenderer.color;
        c.a = 1;
       
        if (b)
            c.a = 1;
        else
            c.a = 0.5f;

        spriteRenderer.color = c;

    }

}
