﻿using UnityEngine;


public class Talon : Champion {

    private Hitbox hitBox;

    public int spellDamage;
    public float spellKnockBack;
    public float spellSpeed;

    private Vector2 spellTargetDirection;

    /*public AudioSource eOnCast;
	public AudioSource rOnCast;
	public AudioSource rLoop;*/

    protected override void Awake() {

        base.Awake();

        hitBox = transform.GetChild(0).GetComponent<Hitbox>();

        nextSpell = float.NegativeInfinity;
    }

    protected override void Update() {

        base.Update();

        if (!LevelManager.instance.CutScene()) {
            CheckStatus();
        } else
            PuppetBehaviour();

        // If he has already finished his spell, he's no more intargettable.
        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Spell") && gameObject.layer == LayerMask.NameToLayer("Intargettable"))
            gameObject.layer = LayerMask.NameToLayer("Player");

    }

    protected override void CheckStatus() {

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Spell"))
            DuringSpell();
        else
            base.CheckStatus();
    }

    protected override void DuringIdle() {

        if (SpellCondition()) { //Spell

            spellTargetDirection = GetSpellTargetDirection() * spellSpeed;

            animator.SetTrigger("Spell");

        } else
            base.DuringIdle();
    }

    private Vector2 GetSpellTargetDirection() {

        return ((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - rigidBody.position).normalized;
    }

    private void SpellOnCast() {

        nextSpell = Time.time + spellCD;

        hitBox.damage = spellDamage;
        hitBox.knockback = spellKnockBack;

        if (spellTargetDirection.x > 0 && !IsFacingRight())
            Flip();
        else if (spellTargetDirection.x < 0 & IsFacingRight())
            Flip();

        // Dashes in the direction of the spell target.
        rigidBody.velocity = spellTargetDirection;

        // Audio.
        //eOnCast.Play();
    }

    private void DuringSpell() {

        if (lastState != "Spell")
            gameObject.layer = LayerMask.NameToLayer("Intargettable");

        lastState = "Spell";

    }


    /*protected override void CheckStatus() {

		if( animator.GetCurrentAnimatorStateInfo(0).IsName("Spell") )
			DuringSpell();
		else if( animator.GetCurrentAnimatorStateInfo(0).IsName("Ulti") )
			DuringUlti();
		else
			base.CheckStatus();
	}

	protected override void DuringIdle() {

		if( Input.GetButtonDown("Spell") )  { //Spell

			animator.SetTrigger("Spell");

		} else if( Input.GetButtonDown("Ulti") )  { //Ulti

			animator.SetTrigger("Ulti");

		} else
			base.DuringIdle();
	}

	private void SpellOnCast() {
		//Blinks forward and flips
		rigidBody.velocity = Vector2.zero; //Speed = 0
		transform.position = new Vector2( transform.position.x + transform.lossyScale.x * 2.5f, transform.position.y );
		Flip();
		faceRight = !faceRight;
		//Audio
		//eOnCast.Play();
	}

	private void DuringSpell() {

		if( Input.GetButtonDown("Ulti") ) {
			animator.SetTrigger("Ulti");
		} else {
			lastState = "Spell";
		}
	}

	private void UltiOnCast() {
		rigidBody.velocity = Vector2.zero;
		//Audio
		//rOnCast.Play();
		//rLoop.Play();
	}

	private void DuringUlti() {

		if( lastState != "Ulti" )
			UltiOnCast();

		if( Input.GetButtonDown("Jump") ) {

			animator.SetTrigger("Jump");

		} else if( Input.GetButtonDown("Vertical")  &&  bodyCollider.IsTouchingLayers(LayerMask.GetMask("Rope Ladder")) ) {

			rigidBody.gravityScale = 0;
			animator.SetBool("Climbing",true);

		} else {
			hInput();
			lastState = "Ulti";
		}
	}

	protected override void DuringJump() {

		if( Input.GetButtonDown("Spell") )  { //Spell

			animator.SetTrigger("Spell");

		} else
			base.DuringJump();
	}

	protected override void DuringFalling() {

		if( Input.GetButtonDown("Spell") )  { //Spell

			animator.SetTrigger("Spell");

		} else
			base.DuringFalling();
	}*/
}


	/*protected override void Update() {

		switch(status) {
		case "":

			if( Input.GetButtonDown("Spell1") && grounded )  { //Spell 1

				status = "Spell1";
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				GetComponent<Animator>().SetTrigger("Spell1");

				//Audio
				/*GetComponent<AudioSource>().clip = q_oc;
				GetComponent<AudioSource>().Play();

			} else if( Input.GetButtonDown("Spell2") )  { //Spell 2

				status = "Spell2";
				GetComponent<Animator>().SetTrigger("Spell2");
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				transform.position = new Vector2( transform.position.x + transform.lossyScale.x * 2.5f, transform.position.y );
				Flip();
				faceRight = !faceRight;

				//Audio
				/*GetComponent<AudioSource>().clip = e_oc;
				GetComponent<AudioSource>().Play();

			} else if( Input.GetButtonDown("Ulti") && grounded )  { //Ulti

				status = "Ulti";
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				GetComponent<Animator>().SetTrigger("Ulti");

				//Audio
				/*GetComponent<AudioSource>().clip = r_oc;
				GetComponent<AudioSource>().Play();
				GetComponent<AudioSource>().clip = r_loop;
				GetComponent<AudioSource>().Play();
			}

			break;
		case "Spell1":

			Spell1();

			break;

		case "Spell2":

			Spell2();

			break;
		case "Ulti":

			Ulti();

			break;
		}

		base.Update();
	}


	protected void Spell1() {

		GetComponent<Rigidbody2D>().velocity = Vector2.zero;

		if( !GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Spell1") )
			status = "";
	}

	protected void Spell2() {

		GetComponent<Rigidbody2D>().velocity = Vector2.zero;

		if( !GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Spell2"))
			status = "";
	}

	protected void Ulti() {

		GetComponent<Rigidbody2D>().velocity = Vector2.zero;

		if( !GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Ulti"))
			status = "";
	}*/
