﻿using UnityEngine;

public class Garen : Champion {

    private Hitbox hitBox;

    public int spellDamage;
    public float spellKnockBack;

    protected override void Awake() {

        base.Awake();

        hitBox = transform.GetChild(0).GetComponent<Hitbox>();
    }

    protected override void Update() {

        base.Update();

        if (!LevelManager.instance.CutScene()) {

            CheckStatus();

        } else
            PuppetBehaviour();
    }

    protected override void CheckStatus() {

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Spell"))
            DuringSpell();
        else
            base.CheckStatus();

    }

    protected override void DuringIdle() {

        if (Input.GetMouseButtonDown(0) && nextSpell <= Time.time) { //Spell

            animator.SetTrigger("Spell");

        } else
            base.DuringIdle();
    }

    private void DuringSpell() {

        if (lastState != "Spell") {
            SpellOnCast();
        }

        lastState = "Spell";

        DirectionInput();

    }

    private void SpellOnCast() {

        nextSpell = Time.time + spellCD;

        hitBox.damage = spellDamage;
        hitBox.knockback = spellKnockBack;

        // Audio.
        //eOnCast.Play();
    }
}
