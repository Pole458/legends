﻿using UnityEngine;

public class GarenBoss : Minion {

    /*
     * Story: first katarina's mission. she wants to demonstate her value, but 
     * Feeling: bulky enemy, danger to stay near him
     * Skill: basic boss mechanics. Attack timing and dodging boss attacks.
     */


    public float decisionTime;

    private float nextDecision;

    private enum State {
        Idle, Spell, Ulti
    }

    private State currentState;

    protected override void Update() {

        base.Update();

        if (HP <= 0) {
            // What should the boss do once defeated?
            // - disable Boss.cs so it can be easily animated
            // - destroy itself
            // - disable the gameObject
            Destroy(gameObject);
        }

        CheckDirection();

        animator.SetBool("Moving", rigidBody.velocity != Vector2.zero);

    }

    protected override void FixedUpdate() {

        // Work In Progress
        // We first have to design the boss battle.

        /*
         * The boss battle begins in the tower, then proceeds on the roof.
         * Garen chases the player, randomly spinning to win.
         * Sometimes he stops and summons a minion wave.
         * When he drops under a certain level of health, he runs to the next floor
         * Every level of the tower, the time between two decision decreases.
         */

        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Hit")) {

            if (PlayerInRange()) {

                Attack();

            } else if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")) {

                MoveToPlayer();

            }

        }
    }

    private void SpinToWin() {

    }

    private void SpawnWave() {

    }
    
}
