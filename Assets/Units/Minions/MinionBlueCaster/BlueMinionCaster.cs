﻿using UnityEngine;
using UnityEngine.Events;
using Utilities;

public class BlueMinionCaster : Minion {

    public BasicSkillshot skillshot;
    public int damage;

    public UnityEvent FireMethod;

    protected override void Awake() {

        base.Awake();

        if (FireMethod == null)
            FireMethod = new UnityEvent();
    }

    public void Fire() {

        FireMethod.Invoke();

    }

    public void FireSingle() {

        // Calculate angle.
        Quaternion newRotation = Quaternion.LookRotation(transform.position - Player.Position() - new Vector3(0, 0.25f), Vector3.forward);
        newRotation.x = 0;
        newRotation.y = 0;

        BasicSkillshot s = Instantiate(skillshot, (Vector2)transform.position + new Vector2(0, 0.25f), newRotation);
        s.OnCreation(damage);
    }

    public void FirePredict() {

        Vector2 impactPosition = (Vector2)Player.Position() + LevelManager.instance.GetCurrentChampion().GetRigidBody().velocity * Vector2.Distance(transform.position, Player.Position()) / skillshot.speed; ;

        Quaternion newRotation = Quaternion.LookRotation((Vector2)transform.position - impactPosition - new Vector2(0, 0.25f), Vector3.forward);
        newRotation.x = 0;
        newRotation.y = 0;

        BasicSkillshot s = Instantiate(skillshot, (Vector2)transform.position + new Vector2(0, 0.25f), newRotation);
        s.OnCreation(damage);

    }
    
    public void FireFan(int proj) {

        // Calculate angle.
        Quaternion newRotation = Quaternion.LookRotation(transform.position - Player.Position() - new Vector3(0, 0.25f), Vector3.forward);
        newRotation.x = 0;
        newRotation.y = 0;

        Quaternion[] rotations = new Quaternion[proj];

        float deltaAngle = 60f / proj;

        for (int i = 0; i < proj; i++) {
                       
            rotations[i] = Quaternion.AngleAxis( -deltaAngle + i * deltaAngle, Vector3.forward ) * newRotation;

            BasicSkillshot s = Instantiate(skillshot, (Vector2)transform.position + new Vector2(0, 0.25f), rotations[i]);
            s.OnCreation(damage);
        }
    }

}
