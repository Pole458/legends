﻿using UnityEngine;
using Utilities;

public class Minion : Unit {

    public float attTimer;
    public float attackRange;

    protected float nextAttack;

    protected Movement movementSystem;

    protected override void Awake() {

        base.Awake();

        nextAttack = Time.time;

        movementSystem = GetComponent<Movement>();

    }

    protected override void Update () {

        base.Update();

        if(animator.GetCurrentAnimatorStateInfo(0).IsName("Hit"))
            CheckDirectionInverted();
        else
            CheckDirection();

        animator.SetBool("Moving", rigidBody.velocity != Vector2.zero);
    }

    protected void LateUpdate() {

        if (HP <= 0 && !animator.GetCurrentAnimatorStateInfo(0).IsName("Hit")) {
            Destroy(gameObject);
        }

    }

    protected virtual void FixedUpdate() {

        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Hit")) {

            if (CanSeePlayer()) {

                if (PlayerInRange()) {

                    Attack();

                } else if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")) {

                    MoveToPlayer();

                }

            } else movementSystem.Move(rigidBody, speed);

        }
    }

    private void OnDestroy() {
        LevelManager.instance.roomManager.AddEnemies(-1);
    }

    protected virtual void Attack() {

        if (Time.time >= nextAttack) {
            FaceTarget(Player.GameObject());
            animator.SetTrigger("Attack");
            nextAttack = Time.time + attTimer;
        }

        rigidBody.velocity = Vector2.zero;
    }

    protected virtual void MoveToPlayer() {

        rigidBody.velocity = ((Vector2)Player.Position() - rigidBody.position).normalized * speed;
    }

    protected virtual bool PlayerInRange() {

        return Vector2.Distance(rigidBody.position, Player.Position()) < attackRange;
    }

    protected virtual bool CanSeePlayer() {

        return spriteRenderer.isVisible && Player.Champion().IsVisible() && !Physics2D.Linecast(transform.position, Player.Position(), LayerMask.GetMask("Walls"));
    }

    public override void TakeDamage(int damage) {

        base.TakeDamage(damage);

        // Hitting a minion prevents him to attack for the next 0.5 sec.
        nextAttack = Mathf.Max( nextAttack, Time.time + 0.5f);
    }

}
