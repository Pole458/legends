﻿using UnityEngine;

public class RandomMovement : Movement {

    // How often this minion should change direction.
    public float time;

    public float deltaTime;

    private float nextTime;

    private void Awake() {

        nextTime = Time.time;

    }

    public override void Move(Rigidbody2D rigidBody, float speed) {

        if( Time.time > nextTime ) {
            rigidBody.velocity = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * speed / 2;
            nextTime = Time.time + time + Random.Range(-deltaTime, deltaTime);
        }

    }
}
