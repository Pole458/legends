﻿using UnityEngine;

public class PathMovement : Movement {

    public Vector2[] path;

    private int targetPoint;

    public bool backAndForth;

    private int direction;

    private void Awake() {
        targetPoint = 0;
        direction = 1;
    }

    public override void Move(Rigidbody2D rigidBody, float speed) {

        if (Vector2.Distance(transform.position, path[targetPoint]) < 0.05f) {
            targetPoint += direction;
            if (backAndForth) {
                if (targetPoint <= -1 && direction == -1) {
                    direction = 1;
                    targetPoint = path.Length - 1;
                } else if (targetPoint >= path.Length && direction == 1) {
                    direction = -1;
                    targetPoint = 0;
                }
            }
        }

        rigidBody.velocity = (path[targetPoint]- rigidBody.position).normalized * speed;

    }

    public void OnDrawGizmosSelected() {

        // Display the path of this moving platform
        Gizmos.color = new Color(0, 1, 0);
        for (short i = 0; i < path.Length - 1; i++)
            Gizmos.DrawLine(path[i], path[i + 1]);

        //If the path is a close line, connect the first and the last point;
        if (!backAndForth)
            Gizmos.DrawLine(path[0], path[path.Length - 1]);
    }
}
