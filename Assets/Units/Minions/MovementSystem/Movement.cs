﻿using UnityEngine;

public class Movement : MonoBehaviour {

    // Call this during the FixedUpdate.
    public virtual void Move(Rigidbody2D rigidBody, float speed) {

    }

}
