﻿using UnityEngine;

public class Unit : MonoBehaviour {

    public string lastState;

    public int HPMax;
    public int HP;

    public float speed;

    protected Rigidbody2D rigidBody;
    protected Animator animator;
    protected Collider2D bodyCollider;
    protected SpriteRenderer spriteRenderer;

    protected virtual void Awake() {

        HP = HPMax;

        // Grabs the main components.
        rigidBody = GetComponent<Rigidbody2D>();
        bodyCollider = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    protected virtual void Update() {

        CheckDepth();

    }

    protected virtual void PuppetBehaviour() {
        CheckDepth();
    }

    protected virtual void CheckDepth() {
        spriteRenderer.sortingOrder = Mathf.RoundToInt(transform.position.y * 100) * -1;
    }

    public virtual void TakeDamage(int damage) {
        animator.SetTrigger("Hit");
        HP -= damage;
    }

    protected void FaceTarget(GameObject t) {

        if (transform.position.x < t.transform.position.x && !IsFacingRight())
            Flip();
        else if (transform.position.x > t.transform.position.x && IsFacingRight())
            Flip();

    }

    public virtual bool IsFacingRight() {
        return transform.localScale.x > 0;
    }

    public float HPPerc() {
        return HP / (float)HPMax;
    }

    protected virtual void CheckDirection() {
        if (rigidBody.velocity.x > 0 && !IsFacingRight()) {
            Flip();
        } else if (rigidBody.velocity.x < 0 && IsFacingRight()) {
            Flip();
        }
    }

    protected void CheckDirectionInverted() {
        if (rigidBody.velocity.x > 0 && IsFacingRight()) {
            Flip();
        } else if (rigidBody.velocity.x < 0 && !IsFacingRight()) {
            Flip();
        }
    }

    public virtual void Flip() {
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

    public Rigidbody2D GetRigidBody() {
        return rigidBody;
    }

    public Collider2D GetCollider2D() {
        return bodyCollider;
    }

    public Animator GetAnimator() {
        return animator;
    }

    public SpriteRenderer GetSpriteRenderer() {
        return spriteRenderer;
    }
}
