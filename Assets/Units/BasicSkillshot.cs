﻿using UnityEngine;

public class BasicSkillshot : Hitbox {

	public float speed;

	public void OnCreation(int d) {

        damage = d;

        GetComponent<Rigidbody2D>().velocity = speed * transform.up.normalized;
	}

	protected override void OnTriggerEnter2D(Collider2D other) {

		if( other.gameObject.layer == LayerMask.NameToLayer("Walls") ) {
			Destroy(gameObject);
		} else {
			if( other.tag == "Player" ) {
                other.GetComponent<Champion>().TakeDamage(damage);
				Destroy(gameObject);
			}
		}
	}

}
