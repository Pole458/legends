﻿using UnityEngine;

public class Hitbox : MonoBehaviour {

    public enum Type { Minion, Player };

    public Type type;

	public int damage;
    public float knockback;

	protected virtual void OnTriggerEnter2D(Collider2D other) {

        switch(type) {
            case Type.Minion:

                Minion m = other.GetComponent<Minion>();

                if (m != null) {
                    m.TakeDamage(damage);
                    KnockBack(m, transform.position, knockback);
                }
                
                break;
            case Type.Player:

                Champion c = other.GetComponent<Champion>();

                if (c != null)
                    c.TakeDamage(damage);

                break;
        }
		
	}

    /// <summary>
    /// Knocks back the "target" unit from the position "pos" with velocity set to "knockback".
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="target"></param>
    /// <param name="knockback"></param>
    protected virtual void KnockBack(Unit target, Vector3 pos, float knockback ) {

        target.GetRigidBody().velocity = (target.transform.position - pos).normalized * knockback;

    }

    // Used for destroy AoE Hitbox.
    public void DestroyOnEndAnimation() {
        Destroy(gameObject);
    }

}
