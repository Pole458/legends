﻿using UnityEngine;

namespace Utilities {

    public class Player {

        public static GameObject GameObject() {
            return LevelManager.instance.GetCurrentChampion().gameObject;
        }

        public static Champion Champion() {
            return LevelManager.instance.GetCurrentChampion();
        }

        public static Vector3 Position() {
            return LevelManager.instance.GetCurrentChampion().transform.position;
        }

    }

}
