﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;

	/// <summary>
	/// The current game status.
	/// </summary>
	public string gameStatus;

	/// <summary>
	/// The scriptObj containing all the general information about the game.
	/// </summary>
	public GameData gameData;

	/// <summary>
	/// The save data file.
	/// </summary>
	public SaveData saveData;

	public LevelData currentLevelData;

	void Awake () {

        if (instance == null) {
            instance = this;

            DontDestroyOnLoad(gameObject);

            // Limits the frame rate of the game.
            Application.targetFrameRate = 60;

            LoadSaveData();

        } else if (instance != this)
            Destroy(gameObject);
    }

	/// <summary>
	/// Loads the given level.
	/// </summary>
	/// <param name="levelData">Level data.</param>
	public void LoadLevel(LevelData levelData) {

		currentLevelData = levelData;

		SceneManager.LoadScene (levelData.name);

		gameStatus = "Level: " + levelData.name + " Mode: Story";

	}

	/// <summary>
	/// Reads the button event from the button listener and decides what to do knowing the gameStatus
	/// </summary>
	public void OnButtonPress(string buttonName) {

		//If it's in the Main Menu
		if (gameStatus == "MainMenu") {

			if (buttonName == "Story") {
				gameStatus = "StoryMenu";
				SceneManager.LoadScene ("StoryMenu");
			} else if (buttonName == "Exit") {
				Application.Quit ();
			
			}
		} else if (gameStatus == "StoryMenu") {

			if (buttonName == "Exit") {
				gameStatus = "MainMenu";
				SceneManager.LoadScene ("MainMenu");
			}
		}
	}


	/// <summary>
	/// Tries to load the SaveData. If the SaveData file does not exists, then creates a new one.
	/// </summary>
	private void LoadSaveData() {

		if (File.Exists (Application.persistentDataPath + "/SaveData.sav")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/SaveData.sav", FileMode.Open);
			saveData = (SaveData)bf.Deserialize (file);
			file.Close ();
		} else {
			saveData = new SaveData ();
			Save ();
			Debug.Log ("No data found, created new data");
		}
			
	}

	/// <summary>
	/// Save the SaveData files into the persistentDataPath.
	/// </summary>
	private void Save() {
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/SaveData.sav");
		bf.Serialize (file, saveData);
		file.Close ();
	}

}
