﻿using UnityEngine;

public class RoomEvent : StoryEvent {

    public GameObject room;

    protected override bool TriggerCondition() {

        return LevelManager.instance.roomManager.GetCurrentRoom() == room;

    }
}
