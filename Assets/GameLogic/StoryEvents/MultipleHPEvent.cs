﻿
public class MultipleHPEvent : StoryEvent {

    public Unit[] units;

    public int HP;

    /// <summary>
    /// True -> All the units should have HP lower
    /// False -> At least one unit should have HP lower
    /// </summary>
    public bool and;

    protected override bool TriggerCondition() {

        if (and) {

            foreach (Unit u in units) {

                if (u != null && HP < u.HP) return false;
            }
 
            return true;

        } else {

            foreach (Unit u in units) {

                if (u == null) return true;

                if (HP > u.HP) return true;
            }

            return false;
        }
    }
}
