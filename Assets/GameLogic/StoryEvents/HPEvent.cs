﻿
public class HPEvent : StoryEvent {

    public Unit unit;

    public int HP;

    protected override bool TriggerCondition() {

        if (unit == null) return true;

        return HP > unit.HP;

    }
}
