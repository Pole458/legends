﻿using Utilities;
using UnityEngine;

public class PositionEvent : StoryEvent {

    public float range;

    protected override bool TriggerCondition() {

        return Vector2.Distance( Player.Position(), transform.position) < range;

    }

}
