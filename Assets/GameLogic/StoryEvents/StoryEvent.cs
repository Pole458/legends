﻿using UnityEngine;

public class StoryEvent : MonoBehaviour {

    public StoryEvent[] nextEvents;

    public int animIndex;

    protected void Start() {

        // Disables all its next events.
        foreach (StoryEvent e in nextEvents)
            e.gameObject.SetActive(true);
    }

    protected void Update() {
       
        if ( TriggerCondition() ) {

            // Enables all the next events.
            foreach (StoryEvent e in nextEvents)
                e.gameObject.SetActive(true);

            // Disables itself.
            gameObject.SetActive(false);

            // Plays the animation triggered by this storyEvent.
            LevelManager.instance.animationData.Play(animIndex);
            
        } 

    }

    protected virtual bool TriggerCondition() {
        return false;
    }

}
