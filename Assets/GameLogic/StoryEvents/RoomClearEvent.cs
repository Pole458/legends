﻿using UnityEngine;

public class RoomClearEvent : StoryEvent {

    public GameObject room;

    protected override bool TriggerCondition() {

        return LevelManager.instance.roomManager.GetCurrentRoom() == room && LevelManager.instance.roomManager.GetCurrentRoomEnemies() == 0;

    }
}
