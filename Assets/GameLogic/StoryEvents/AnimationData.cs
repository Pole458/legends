﻿using UnityEngine;
using DG.Tweening;

public class AnimationData : MonoBehaviour {

    public virtual void Play(int i) {

    }

    protected Tween MoveUnit(Unit u, Vector2 to) {

        return u.GetRigidBody().DOMove(to, Vector2.Distance(u.GetRigidBody().position, to) / u.speed).OnComplete(() => {
            u.GetAnimator().SetBool("Moving", false);
            u.GetRigidBody().velocity = Vector2.zero;
        }).SetEase(Ease.Linear).OnUpdate(() => {
            u.GetAnimator().SetBool("Moving", Vector2.Distance(u.transform.position, to) > 0.1f);
            if (transform.position.x > to.x && u.IsFacingRight())
                u.Flip();
            else if (transform.position.x < to.x && !u.IsFacingRight())
                u.Flip();
        }); 
    }
}
