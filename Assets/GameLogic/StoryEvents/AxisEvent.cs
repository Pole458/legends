﻿using Utilities;

public class AxisEvent : StoryEvent {

    public enum Axis { x, y };

    public enum Direction { Less, Greather };

    public Axis axis;

    public Direction direction;

    protected override bool TriggerCondition() {

        if( axis == Axis.x ) {

            if( direction == Direction.Less ) {
                return Player.Position().x < transform.position.x;
            } else {
                return Player.Position().x > transform.position.x;
            }

        } else {

            if (direction == Direction.Less) {
                return Player.Position().y < transform.position.y;
            } else {
                return Player.Position().y > transform.position.y;
            }

        }
    }

}
