﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelLoader : MonoBehaviour {

	public static LevelLoader instance = null;

	public RectTransform valoranMap;
	public GameObject levelIconPrefab;
	public ButtonListener buttonListener;

	public List<LevelData> levelsDataLoaded;

	public void Awake() {
	
		if (instance == null) {

			instance = this;

			LoadLevelIcons ();

		} else if (instance != this)
			Destroy (gameObject);

	}

	/// <summary>
	/// Loads the level icons on the map in the Story Menu.
	/// </summary>
	private void LoadLevelIcons() {

		LevelData tempLevel;
		GameObject tempIcon; // A temporary object created to set the position of the new Level icon.
		string levelName;

		//For each saga...
		for (short i = 0; i < GameManager.instance.gameData.sagasNames.Length; i++) {

			//...if that saga is unlocked, load the level reached
			if (GameManager.instance.saveData.levelsUnlocked [i] != -1) {

                //The name of the level to load, (example: SagaName0, AnOtherSagaName3)
                levelName = GameManager.instance.gameData.sagasNames[i] + GameManager.instance.saveData.levelsUnlocked[i];

                //Loads the LevelData of the level indicated by the SaveData
                tempLevel = (LevelData)Resources.Load("LevelData/" + levelName);

                if (tempLevel == null)
                    Debug.Log("Failed to load " + levelName);

				//Creates the new Level Icon
				tempIcon = Instantiate (levelIconPrefab, valoranMap, false) as GameObject;
				//Positions the new Level Icon
				tempIcon.GetComponent<RectTransform>().localPosition = tempLevel.iconPosition;

				//Links the new Level Icon to the Button Listener
				//tempIcon.GetComponent<Button>().onClick.AddListener(() => buttonListener.OnButtonPress("Load" + levelName));
				tempIcon.GetComponent<Button>().onClick.AddListener(() => GameManager.instance.LoadLevel(tempLevel));

				//Adds the level just loaded into the list of Loaded level;
				levelsDataLoaded.Add (tempLevel);
			
			}

		}
	}

}
