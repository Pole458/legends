﻿using UnityEngine;

public class CameraController : MonoBehaviour {

	private GameObject target;

	public float dampTime;

    private Vector3 moveSpeed;

    public Vector3 offset;

	/// <summary>
	/// The pixels per unit.
	/// </summary>
	public short pixelsPerUnit;
	
    /// <summary>
	/// The zoom factor, modify this to set the camera to the right resolution
	/// </summary>
	public int zoomFactor;

	/// <summary>
	/// The target resolution (Height in pixels) used in the Editor to preview the camera size
	/// </summary>
	public short targetResolution;

	[Range(-2, 2)] public int offsetNumber;

	//To keep track of what has changed when resizing.
	private int oldZoomFactor;
	private int oldTargetResolution;

	public Vector2 topLeftEdge;
	public Vector2 botRightEdge;

	private Vector3 targetPosition;

	private Camera thisCamera;

	private void Awake() {

        //Grabs the camera
        thisCamera = GetComponent<Camera>();

        offset = new Vector2(0, 0.5f);

        SetPixelPerfectSize();

        targetPosition = transform.position;

        target = null;
	
		oldZoomFactor = zoomFactor;
		oldTargetResolution = targetResolution;

	}

	private void FixedUpdate () {

        if(!LevelManager.instance.CutScene()) {

            CalculateTargetPosition();

            Move();

        }
		
	}

    private void LateUpdate() {

        SnapCam();

        Edge();

        // Lock Z axis.
        transform.position = new Vector3(transform.position.x, transform.position.y, -10);
    }


    private void CalculateTargetPosition() {

        if (target != null) {

            targetPosition = target.transform.position + offset + new Vector3(Mathf.Min(Mathf.Max(Input.mousePosition.x / Screen.width, 0), 1) - 0.5f, Mathf.Min(Mathf.Max(Input.mousePosition.y / Screen.height, 0), 1) - 0.5f) * 2;

            targetPosition.z = transform.position.z;

        } else {

            // If no target is found, the camera will go to (0,0,0) 
            target = GameManager.instance.gameObject;

        }
    }

    private void Move() {

        // Smooth.
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref moveSpeed, dampTime);

        // Direct.
        //transform.position = targetPosition;
    }

	private void Edge() {
		
        //Right edge
		if(thisCamera.orthographicSize*thisCamera.aspect + transform.position.x > botRightEdge.x)
			transform.position = new Vector3( botRightEdge.x - thisCamera.orthographicSize*thisCamera.aspect,
			                                 transform.position.y,
			                                 transform.position.z);
		//Left edge
		if(-thisCamera.orthographicSize*thisCamera.aspect + transform.position.x < topLeftEdge.x)
			transform.position = new Vector3( topLeftEdge.x + thisCamera.orthographicSize*thisCamera.aspect,
			                                 transform.position.y,
			                                 transform.position.z);

		//Top edge
		if(thisCamera.orthographicSize + transform.position.y > topLeftEdge.y)
			transform.position = new Vector3( transform.position.x,
			                                 topLeftEdge.y - thisCamera.orthographicSize,
			                                 transform.position.z);
		//Bottom edge
		if(-thisCamera.orthographicSize + transform.position.y < botRightEdge.y)
			transform.position = new Vector3( transform.position.x,
			                                 botRightEdge.y + thisCamera.orthographicSize,
			                                 transform.position.z);
	}

	public void SetTargetPosition(Vector2 pos) {
		targetPosition = new Vector3 (pos.x, pos.y, transform.position.z);
	}

	public void SetTargetTransform(GameObject t) {
        target = t;
	}

	private void SetPixelPerfectSize() {

		if (Application.isEditor) {
			//Set the camera to the right Orthographic Pixel Perfect value in the Editor
			GetComponent<Camera>().orthographicSize = targetResolution / 2f / pixelsPerUnit;
		} else {
			//Set the camera to the right Orthographic Pixel Perfect value
			GetComponent<Camera>().orthographicSize = Screen.height / 2f / pixelsPerUnit;
		}

		if( zoomFactor > 1 )
			GetComponent<Camera>().orthographicSize /= zoomFactor;
	}

	private void SnapCam ( ) {
		Vector3 newPos = transform.position;
		newPos.x =  (Mathf.RoundToInt(newPos.x*pixelsPerUnit*zoomFactor) + ((float)offsetNumber/4) ) / (pixelsPerUnit*zoomFactor);
		newPos.y =  (Mathf.RoundToInt(newPos.y*pixelsPerUnit*zoomFactor)) / (float)(pixelsPerUnit*zoomFactor);
		transform.position = newPos;
	}
    
    public Bounds GetBounds() {
        return new Bounds(new Vector3(transform.position.x, transform.position.y, 0), new Vector2(thisCamera.orthographicSize * thisCamera.aspect, thisCamera.orthographicSize) * 2.2f); // <- 1.1 * 2
    }

    //Gizmos
    public void OnDrawGizmosSelected() {

		// Display the boundaries of this camera
		Gizmos.color = new Color(0, 1, 0);
		//Top
		Gizmos.DrawLine (new Vector3 (topLeftEdge.x, topLeftEdge.y, transform.position.z), new Vector3 (botRightEdge.x, topLeftEdge.y, transform.position.z));
		//Left
		Gizmos.DrawLine (new Vector3 (topLeftEdge.x, topLeftEdge.y, transform.position.z), new Vector3 (topLeftEdge.x, botRightEdge.y, transform.position.z));
		//Right
		Gizmos.DrawLine (new Vector3 (botRightEdge.x, topLeftEdge.y, transform.position.z), new Vector3 (botRightEdge.x, botRightEdge.y, transform.position.z));
		//Bot
		Gizmos.DrawLine (new Vector3 (topLeftEdge.x, botRightEdge.y, transform.position.z), new Vector3 (botRightEdge.x, botRightEdge.y, transform.position.z));

        //Center
        Gizmos.color = new Color(1, 1, 1);
        Gizmos.DrawLine(transform.position + new Vector3(0, 0.5f), transform.position + new Vector3(0, -0.5f));
        Gizmos.DrawLine(transform.position + new Vector3(0.5f, 0), transform.position + new Vector3(-0.5f, 0));

        /*// Display the boundaries of the window
		Gizmos.color = new Color(1, 1, 1);
		//Top
		Gizmos.DrawLine (new Vector3 (topLeftWindow.x + transform.position.x, topLeftWindow.y + transform.position.y, transform.position.z), new Vector3 (botRightWindow.x + transform.position.x, topLeftWindow.y + transform.position.y, transform.position.z));*/
    }

	void OnValidate() {
		if (zoomFactor != oldZoomFactor || targetResolution != oldTargetResolution) {
			oldZoomFactor = zoomFactor;
			oldTargetResolution = targetResolution;
            SetPixelPerfectSize();
		}
	}

}
