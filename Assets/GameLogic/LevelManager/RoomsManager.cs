﻿using UnityEngine;
using Utilities;

public class RoomManager {

    /// <summary>
    /// The rooms that compose this level.
    /// </summary>
    private GameObject[] rooms;

    /// <summary>
    /// This indicates the number of enemies in this room.
    /// </summary>
    private int currentRoomEnemies;

    /// <summary>
    /// The collider of all the rooms floors, used to understand the current room.
    /// </summary>
    private Collider2D[] floorBounds;

    /// <summary>
    /// The bounds of all the rooms, used to acces it even when disabled.
    /// </summary>
    private Bounds[] roomsBounds;

    /// <summary>
    /// The room in which the player currenly is.
    /// </summary>
    private GameObject currentRoom;

    public RoomManager(Transform levelTransform) {

        int roomsCount = 0;

        // Count how many rooms compose this level.
        foreach (Transform child in levelTransform) {
            if (child.tag == "Room")
                roomsCount++;
        }

        rooms = new GameObject[roomsCount];
        floorBounds = new Collider2D[roomsCount];
        roomsBounds = new Bounds[roomsCount];
        currentRoomEnemies = 0;

        currentRoom = null;

        int i = 0;
        foreach (Transform levelElement in levelTransform) {

            if (levelElement.tag == "Room") {

                rooms[i] = levelElement.gameObject;
                floorBounds[i] = levelElement.Find("Floor").GetComponent<Collider2D>();
                roomsBounds[i] = levelElement.Find("Walls").GetComponent<Collider2D>().bounds;
                roomsBounds[i].Encapsulate(floorBounds[i].bounds);

                // Check if this is the room containing the player.
                if (roomsBounds[i].Contains(Player.Position())) {
                    currentRoom = rooms[i];
                    CountEnemies();
                } else {
                    // If this is not the current room, do not disable the minions and the door.
                    foreach (Transform roomElement in levelElement) {
                        if (roomElement.tag == "Minion")
                            roomElement.GetComponent<Minion>().enabled = false;
                        else if (roomElement.tag == "Door")
                            roomElement.gameObject.SetActive(false);
                    }
                }
                i++;
            }
        } 
    }
	
	public void Update(Bounds cameraBounds) {
        
        for (int i = 0; i < rooms.Length; i++) {

            if (roomsBounds[i].Intersects(cameraBounds)) {

                if (!rooms[i].activeSelf)
                    rooms[i].SetActive(true);

                if (rooms[i] == currentRoom) {

                    SetDoors(currentRoom.transform, currentRoomEnemies == 0);

                } else {

                    if (currentRoomEnemies == 0 && RoomContainsPlayer(i)) {
                        SetAsCurrentRoom(rooms[i]);
                    }
                }

            } else
                if (rooms[i].activeSelf)
                rooms[i].SetActive(false);
        }
    }

    private void SetAsCurrentRoom(GameObject room) {

        currentRoom = room;

        // Enables all the enemies in the room.
        foreach (Transform child in currentRoom.transform) {
            if (child.tag == "Minion")
                child.GetComponent<Minion>().enabled = true;
        }

        CountEnemies();

        SetDoors(currentRoom.transform, currentRoomEnemies == 0);

    }

    /// <summary>
    /// Counts the enemies in the current room.
    /// </summary>
    private void CountEnemies() {

        currentRoomEnemies = 0;

        foreach (Transform child in currentRoom.transform) {
            if (child.tag == "Minion")
                currentRoomEnemies++;
        }
    }

    /// <summary>
    /// Enables/Disables the door of a room. True stands for open doors.
    /// </summary>
    /// <param name="room"></param>
    /// <param name="b"></param>
    private void SetDoors(Transform room, bool b) {

        foreach (Transform child in room) {
            if (child.tag == "Door")
                child.gameObject.SetActive(!b);
        }

    }

    private bool RoomContainsPlayer(int i) {
        return floorBounds[i].OverlapPoint(new Vector2(Player.Champion().GetCollider2D().bounds.max.x, Player.Position().y)) &&
            floorBounds[i].OverlapPoint(new Vector2(Player.Champion().GetCollider2D().bounds.min.x, Player.Position().y)) &&
            floorBounds[i].OverlapPoint(new Vector2(Player.Position().x, Player.Champion().GetCollider2D().bounds.max.y)) &&
            floorBounds[i].OverlapPoint(new Vector2(Player.Position().x, Player.Champion().GetCollider2D().bounds.min.y));
    }

    /// <summary>
    /// Call this whether the number of enemies changes.
    /// </summary>
    /// <param name="e"></param>
    public void AddEnemies(int e) {
        currentRoomEnemies = Mathf.Max(0, currentRoomEnemies + e);
    }

    public GameObject GetCurrentRoom() {
        return currentRoom;
    }

    public int GetCurrentRoomEnemies() {
        return currentRoomEnemies;
    }
}
