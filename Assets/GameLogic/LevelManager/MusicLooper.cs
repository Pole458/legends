﻿using UnityEngine;

[System.Serializable]
public class MusicLooper {

    /// <summary>
    /// The clip to loop.
    /// </summary>
    public AudioSource music;

    /// <summary>
    /// The starting sample of the loop.
    /// </summary>
    public int loopStart;

    /// <summary>
    /// The ending sample of the loop.
    /// </summary>
    public int loopEnd;

    public void Update() {
        
        if (music.timeSamples > loopEnd) {
            music.timeSamples = loopStart;
        }
    }

}
