﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance = null;

    /// <summary>
    /// The list of championData the player can control in this level.
    /// </summary>
    public LevelData levelData;

    /// <summary>
    /// The list of champions prefab the player can control in this level.
    /// </summary>
    public Champion[] championPool;

    /// <summary>
    /// The index of the current champion controlled by the player.
    /// </summary>
    public int currentChampion = 0;

    public CameraController cameraController;

	public GUIController guiController;

    public BalloonManager balloon;

    /// <summary>
    /// Helping class for looping the background music
    /// </summary>
    public MusicLooper backgroundMusicLooper;

    /// <summary>
    /// Helping class that enables/disables part of the level.
    /// </summary>
    public RoomManager roomManager;

    /// <summary>
    /// The current sequence used to animate the level.
    /// </summary>
    public Sequence mainSequence = null;

    /// <summary>
    /// Monobehaviour containing all the animations for this level.
    /// </summary>
    public AnimationData animationData;

    /// <summary>
    /// True during cutscenes, false otherwise.
    /// </summary>
    public bool cutScene = false;

    // Used by the animation system 
    public bool canUseSpell = true;
    private bool canChangeChampion = true;
    private bool canMove = true;

    // Prevents player HP to drop below a certain level.
    public int HPThreshold;

    private void Awake() {

        if (instance == null) {

            instance = this;

        } else if (instance != this)
            Destroy(gameObject);

    }

    private void Start () {

        // Gets info about this level.
        if (GameManager.instance != null)
            GetLevelInfo();

        // Grabs the camera controller.
        cameraController = Camera.main.GetComponent<CameraController>();

        // Tells the camera controller to follow the current champion.
        cameraController.SetTargetTransform(championPool[currentChampion].gameObject);

        // Grabs all the rooms in the level.
        roomManager = new RoomManager(transform);

        // Initialize the GUI controller.
        guiController = new GUIController();

        // Calls the animation #0.
        if (GameManager.instance.gameStatus.Contains("Story"))
            animationData.Play(0);

        }

    /// <summary>
    /// Gets the level info from the GameManager.
    /// </summary>
    private void GetLevelInfo() {

        if (GameManager.instance.gameStatus.Contains("Story")) {

        } else {
            // if this level was loaded in the "free game" mode, then the LevelManager grabs the levelData from the GameManager.
            // CreateChampionPool();

        }

        /*championPool = new Champion[GameManager.instance.currentLevelData.championData.Length];
	
		// Instantiate all the champions, stores them in the "championPool" array and places them in the "Player" transform.
		for (short i = 0; i < championPool.Length; i++)
            championPool[i] = (Instantiate(GameManager.instance.currentLevelData.championData[i].prefab, transform) as GameObject).GetComponent<Champion>();*/

    }

    private void Update() {
    
        // Un-pause the sequence with a click when it's paused.
        if(cutScene) {
            if(!mainSequence.IsPlaying()) {
                if (Input.GetMouseButton(0))
                    mainSequence.Play();
            }
        }

        // Check whether to disable/enable the rooms.
        roomManager.Update(cameraController.GetBounds());

        // Update GUI.
        guiController.Update();

        // Loops background music.
        backgroundMusicLooper.Update();

    }
    
   /* private void CreateChampionPool() {

        // This method will be called when starting a level in free mode.

        // All the champions in the champion pool must be destroyed, and the new LevelData must be read from the GameManager.

        championPool = new Champion[levelData.championData.Length];

        actors = new Transform[actorControllers.Length];

        // Instantiate all the champions, stores them in the "championPool" array and places them in the "Player" transform.
        for (int i = 0; i < championPool.Length; i++) {
            championPool[i] = (Instantiate(levelData.championData[i].prefab, transform) as GameObject).GetComponent<Champion>();

            // Add to the actor list every champion that needs to be animated.
            for(int j= 0; j<actorControllers.Length; j++)
                if (championPool[i].name.Contains(actorControllers[j].name)) {
                    actors[j] = championPool[i].transform;
                    break;
                }
        }
    }*/

    public void OnBeginAnimation() {

        cutScene = true;

        // Disables the GUI.
        guiController.gameObject.SetActive(false);

    }

    public void OnEndAnimation() {

        cutScene = false;

        // Enables the GUI.
        guiController.gameObject.SetActive(true);

    }

    public void SetCameraPosition(string pos, float time) {

        Vector2 v;

        v.x = float.Parse(pos.Split(',')[0]);
        v.y = float.Parse(pos.Split(',')[1]);

        cameraController.SetTargetPosition(v);
        //cameraController.dampTime = Vector2.Distance(cameraController.transform.position, v) / 5;

    }

    public bool CanUseSpell() {
        return canUseSpell;
    }

    public void SetSpellUse(bool b) {

        canUseSpell = b;

        guiController.spellIcon.gameObject.SetActive(b);
    }

    public bool CanChangeChampion() {
        return canChangeChampion;
    }

    public void SetChangeChampion(bool b) {

        canChangeChampion = b;

        for(int i = 0; i < championPool.Length - 1; i++) {
            guiController.championHUDs[i].gameObject.SetActive(b);
        }

    }

    public bool CanMove() {
        return canMove;
    }

    public void setMove(bool b) {
        canMove = b;
    }

    public Champion GetCurrentChampion() {
        return championPool[currentChampion];
    }

    public bool CutScene() {
        return cutScene;
    }

}
